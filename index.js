var dsnv = [];
var dsnvJson = localStorage.getItem("DSNV");
if (dsnvJson != null) {
    dsnv = JSON.parse(dsnvJson).map(function (item) {
        return new NhanVien(item.account, item.name, item.email, item.password, item.startdate, item.salary, item.title, item.worktime)
    });
}

renderDSNV(dsnv);

function themNV() {
    var newNV = layThongTinTuForm();

    // validate
    var invalidAccount = [];
    if (newNV.account.length === 0) {
        invalidAccount.push('Ko bỏ trống');
    }
    if (newNV.account.length < 4 || newNV.account.length > 6) {
        invalidAccount.push('4-6 ký tự');
    }

    var invalidName = [];
    if (newNV.name.length === 0) {
        invalidName.push('Ko bỏ trống');
    }
    if (!newNV.name.match(/^[a-z A-Z]+$/)) {
        invalidName.push('Phải là chữ');
    }

    var invalidEmail = [];
    if (newNV.email.length === 0) {
        invalidEmail.push('Ko bỏ trống');
    }
    if (!newNV.email.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
        invalidEmail.push('Sai định dạng');
    }

    var invalidStartDate = [];
    if (newNV.startdate.length === 0) {
        invalidStartDate.push('Ko bỏ trống');
    }
    if (!newNV.startdate.match(/^(0?[1-9]|1[012])[\/](0?[1-9]|[12][0-9]|3[01])[\/]\d{4}$/)) {
        invalidStartDate.push('Sai định dạng mm/dd/yyyy');
    }

    var invalidSalary = [];
    if (newNV.salary.length === 0) {
        invalidSalary.push('Ko bỏ trống')
    }
    if (!(newNV.salary * 1) || newNV.salary * 1 < 1000000 || newNV.salary * 1 > 20000000) {
        invalidSalary.push('Từ 1000000 đến 20000000')
    }

    var invalidTitle = [];
    if (newNV.title == '0') {
        invalidTitle.push('Chưa chọn');
    }

    var invalidWorkTime = [];
    if (newNV.worktime.length === 0) {
        invalidWorkTime.push('Ko bỏ trống');
    }
    if (!(newNV.worktime * 1) || newNV.worktime * 1 < 80 || newNV.worktime * 1 > 200) {
        invalidWorkTime.push('Từ 80 đến 200');
    }

    var invalid = invalidAccount.length || invalidName.length || invalidEmail.length || invalidStartDate.length || invalidSalary.length || invalidWorkTime.length || invalidTitle.length;

    if (invalid) {
        var mess;
        mess = invalidAccount.shift() || '';
        document.getElementById('tbTKNV').innerText = mess;
        document.getElementById('tbTKNV').classList[mess ? 'add' : 'remove']('error')

        mess = invalidName.shift() || '';
        document.getElementById('tbTen').innerText = mess;
        document.getElementById('tbTen').classList[mess ? 'add' : 'remove']('error')

        mess = invalidEmail.shift() || '';
        document.getElementById('tbEmail').innerText = mess;
        document.getElementById('tbEmail').classList[mess ? 'add' : 'remove']('error')

        mess = invalidStartDate.shift() || '';
        document.getElementById('tbNgay').innerText = mess;
        document.getElementById('tbNgay').classList[mess ? 'add' : 'remove']('error')

        mess = invalidSalary.shift() || '';
        document.getElementById('tbLuongCB').innerText = mess;
        document.getElementById('tbLuongCB').classList[mess ? 'add' : 'remove']('error')

        mess = invalidTitle.shift() || '';
        document.getElementById('tbChucVu').innerText = mess;
        document.getElementById('tbChucVu').classList[mess ? 'add' : 'remove']('error')

        mess = invalidWorkTime.shift() || '';
        document.getElementById('tbGiolam').innerText = mess;
        document.getElementById('tbGiolam').classList[mess ? 'add' : 'remove']('error')

        return
    }
    dsnv.push(newNV);


    // Tao JSON
    var dsnvJson = JSON.stringify(dsnv);

    // luu vao local storage

    localStorage.setItem("DSNV", dsnvJson)

    renderDSNV(dsnv);
}

function xoa(index) {
    dsnv.splice(index, 1)
    localStorage.setItem("DSNV", JSON.stringify(dsnv))
    renderDSNV(dsnv);
}

function tim() {
    var xepLoai = document.getElementById('searchName').value
    if (xepLoai) {
        var kq = dsnv.filter(function (item) {
            return item.xepLoai().toLowerCase() === xepLoai.trim().toLowerCase();
        })
        renderDSNV(kq)
    } else {
        renderDSNV(dsnv)
    }
}
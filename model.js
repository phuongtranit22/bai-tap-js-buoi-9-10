// tao lop doi tuong

class NhanVien {
    account;
    name;
    email;
    password;
    startdate;
    title;
    salary;
    worktime;
    constructor(account, name, email, password, startdate, salary, title, worktime) {
        this.account = account;
        this.name = name;
        this.password = password;
        this.title = title;
        this.startdate = startdate;
        this.salary = salary;
        this.worktime = worktime;
        this.email = email;
    }

    tinhTongLuong() {
        var tongLuong = 0;
        switch (this.title) {
            case 'sep':
                tongLuong = this.salary * 3;
                break;
            case 'tp':
                tongLuong = this.salary * 2;
                break;
            case 'nv':
                tongLuong = this.salary * 1;
                break;
        }
        return tongLuong;
    }

    xepLoai() {
        var ketQua = "Trung bình";
        if (this.worktime >= 160) {
            ketQua = "Khá"
        }
        if (this.worktime >= 176) {
            ketQua = 'Giỏi'
        }
        if (this.worktime >= 192) {
            ketQua = 'Xuất sắc'
        }
        return ketQua;
    }
}
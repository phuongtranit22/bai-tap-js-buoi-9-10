function layThongTinTuForm() {
    var taiKhoan = document.getElementById('tknv').value;
    var ten = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var matKhau = document.getElementById('password').value;
    var ngayLam = document.getElementById('datepicker').value;
    var luongCB = document.getElementById('luongCB').value * 1;
    var chucVu = document.getElementById('chucvu').value;
    var gioLam = document.getElementById('gioLam').value * 1;

    return new NhanVien(taiKhoan, ten, email, matKhau, ngayLam, luongCB, chucVu, gioLam);
}



function renderDSNV(dsnv) {
    var contentHTML = "";

    for (var i = 0; i < dsnv.length; i++) {
        var nv = dsnv[i];
        var trContent = `
        <tr>
        <td>${nv.account}</td>
        <td>${nv.name}</td>
        <td>${nv.email}</td>
        <td>${nv.startdate}</td>
        <td>${nv.title}</td>
        <td>${nv.tinhTongLuong()}</td>
        <td>${nv.xepLoai()}</td>
        <td>
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="xoa(${i})">Xoá</button>
        </td>
        </tr>`

        contentHTML += trContent;
    }
    document.getElementById('tableDanhSach').innerHTML = contentHTML;
}

